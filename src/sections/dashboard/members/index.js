import React from "react";
import { ToggleButton, ToggleButtonGroup, Avatar } from "@mui/material";
import ImageIcon from "@mui/icons-material/Image";
import MainPageHeading from "../../../utils/enums";
import { DataGrid } from "@mui/x-data-grid";
import LaptopIcon from "@mui/icons-material/Laptop";
import PersonIcon from "@mui/icons-material/Person";
import PhoneAndroidIcon from "@mui/icons-material/PhoneAndroid";
import MessageIcon from "@mui/icons-material/Message";
import AddMember from "./addmember";
class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      view_type: "table",
      add_member_model: false,
    };
  }

  handleViewChange = (name,view) => {
    this.setState({ [name]: view});
  };

  toggleModel=()=>
  {

  }

  render() {
    const { view_type, add_member_model } = this.state;
    return (
      <>
        {add_member_model ? <AddMember closeModel={this.handleViewChange} /> : null}

        <div className="row">
          <div className="col-12 col-md-8 col-lg-8 col-sm-12">
            {MainPageHeading(
              "Members",
              "Your added members will be shown here in recent order"
            )}
          </div>
          <div className="col-12 col-md-4 col-lg-4 col-sm-12">
            <button
              className="btn ms-3 mt-1 float-end button-primary"
              onClick={()=>this.handleViewChange("add_member_model",!this.state.add_member_model)}
            >
              Add Member
            </button>
            <div className="float-end">
              <ToggleButtonGroup
                aria-label="device"
                onChange={(event, value) => this.handleViewChange("view_type",value[0])}
              >
                <ToggleButton
                  selected={view_type == "table" ? true : false}
                  value="table"
                  name="table"
                  aria-label="table"
                >
                  <LaptopIcon />
                </ToggleButton>
                <ToggleButton
                  selected={view_type == "phone" ? true : false}
                  value="phone"
                  name="phone"
                  aria-label="phone"
                >
                  <PhoneAndroidIcon />
                </ToggleButton>
              </ToggleButtonGroup>
            </div>
          </div>
        </div>
        <div className="row p-2">
          {view_type == "table" ? (
            <div className="col-12 p-0 m-0">
              <div
                style={{ height: 400, width: "100%", backgroundColor: "white" }}
              >
                <DataGrid
                  rows={rows}
                  columns={columns}
                  pageSize={5}
                  rowsPerPageOptions={[25]}
                  density="standard"
                  checkboxSelection
                />
              </div>
            </div>
          ) : (
            <>
              <div className="col-12 col-lg-2 col-md-2 col-sm-12 bg-white borderRadius15 mb-2 me-2">
                <div className="centerAlign pt-3">
                  <div className="mt-2 mb-2">
                    <Avatar
                      alt="Bilal Asghar"
                      src="/static/images/avatar/1.jpg"
                      sx={{ width: 86, height: 86 }}
                    />
                  </div>
                  <h5 className="member_profileCard_title">Bilal Asghar</h5>
                  <p className="member_profileCard_subtitle">Owner</p>
                  <div className="d-flex w-100 member_profile_footer">
                    <button className="btn button-primary-outlined mb-2 me-1">
                      <PersonIcon /> Profile
                    </button>
                    <button className="btn button-primary-outlined mb-2">
                      <MessageIcon />
                    </button>
                  </div>
                </div>
              </div>
              <div className="col-12 col-lg-2 col-md-2 col-sm-12 bg-white borderRadius15 mb-2 me-2">
                <div className="centerAlign pt-3">
                  <div className="mt-2 mb-2">
                    <Avatar
                      alt="Bilal Asghar"
                      src="/static/images/avatar/1.jpg"
                      sx={{ width: 86, height: 86 }}
                    />
                  </div>
                  <h5 className="member_profileCard_title">Bilal Asghar</h5>
                  <p className="member_profileCard_subtitle">Owner</p>
                  <div className="d-flex w-100 member_profile_footer">
                    <button className="btn button-primary-outlined mb-2 me-1">
                      <PersonIcon /> Profile
                    </button>
                    <button className="btn button-primary-outlined mb-2">
                      <MessageIcon />
                    </button>
                  </div>
                </div>
              </div>
              <div className="col-12 col-lg-2 col-md-2 col-sm-12 bg-white borderRadius15 mb-2 me-2">
                <div className="centerAlign pt-3">
                  <div className="mt-2 mb-2">
                    <Avatar
                      alt="Bilal Asghar"
                      src="/static/images/avatar/1.jpg"
                      sx={{ width: 86, height: 86 }}
                    />
                  </div>
                  <h5 className="member_profileCard_title">Bilal Asghar</h5>
                  <p className="member_profileCard_subtitle">Owner</p>
                  <div className="d-flex w-100 member_profile_footer">
                    <button className="btn button-primary-outlined mb-2 me-1">
                      <PersonIcon /> Profile
                    </button>
                    <button className="btn button-primary-outlined mb-2">
                      <MessageIcon />
                    </button>
                  </div>
                </div>
              </div>
            </>
          )}
        </div>
      </>
    );
  }
}

const rows = [
  { id: 1, lastName: "Snow", firstName: "Jon", age: 35 },
  { id: 2, lastName: "Lannister", firstName: "Cersei", age: 42 },
  { id: 3, lastName: "Lannister", firstName: "Jaime", age: 45 },
  { id: 4, lastName: "Stark", firstName: "Arya", age: 16 },
  { id: 5, lastName: "Targaryen", firstName: "Daenerys", age: null },
  { id: 6, lastName: "Melisandre", firstName: null, age: 150 },
  { id: 7, lastName: "Clifford", firstName: "Ferrara", age: 44 },
  { id: 8, lastName: "Frances", firstName: "Rossini", age: 36 },
  { id: 9, lastName: "Roxie", firstName: "Harvey", age: 65 },
  { id: 10, lastName: "Roxie", firstName: "Harvey", age: 65 },
  { id: 11, lastName: "Roxie", firstName: "Harvey", age: 65 },
  { id: 12, lastName: "Roxie", firstName: "Harvey", age: 65 },
  { id: 13, lastName: "Roxie", firstName: "Harvey", age: 65 },
  { id: 14, lastName: "Roxie", firstName: "Harvey", age: 65 },
  { id: 15, lastName: "Roxie", firstName: "Harvey", age: 65 },
];

const columns = [
  { field: "id", headerName: "ID", width: 20 },
  { field: "firstName", headerName: "First name", width: 100 },
  { field: "lastName", headerName: "Last name", width: 110 },
  {
    field: "age",
    headerName: "Age",
    type: "number",
    width: 90,
  },
  {
    field: "fullName",
    headerName: "Full name",
    description: "This column has a value getter and is not sortable.",
    sortable: false,
    width: 160,
    valueGetter: (params) =>
      `${params.getValue(params.id, "firstName") || ""} ${
        params.getValue(params.id, "lastName") || ""
      }`,
  },
];

export default index;

import React from "react";
import {
  TextField,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  InputAdornment,
  IconButton,
} from "@mui/material";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
class addmember extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show_password: false,
    };
  }

  render() {
    const { show_password } = this.state;
    return (
      <>
        <div
          class="modal fade show"
          id="exampleModal"
          tabindex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
          style={{ display: "block", backgroundColor: "#00000047" }}
        >
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                  Add Member
                </h5>
                <button
                  type="button"
                  class="btn-close"
                  onClick={() =>
                    this.props.closeModel("add_member_model", false)
                  }
                ></button>
              </div>
              <div class="modal-body">
                <div className="row">
                  <div className="col-md-6 col-sm-12 col-lg-6 col-12 mt-3">
                    <TextField
                      id="outlined-basic"
                      label="First Name"
                      variant="outlined"
                      sx={{ minWidth: "100%",height:"20px" }}
                    />
                  </div>
                  <div className="col-md-6 col-sm-12 col-lg-6 col-12 mt-3">
                    <TextField
                      id="outlined-basic"
                      label="Last Name"
                      variant="outlined"
                      sx={{ minWidth: "100%" }}
                    />
                  </div>
                  <div className="col-md-6 col-sm-12 col-lg-6 col-12 mt-3">
                    <TextField
                      id="outlined-basic"
                      label="Nick Name"
                      variant="outlined"
                      sx={{ minWidth: "100%" }}
                    />
                  </div>
                  <div className="col-md-6 col-sm-12 col-lg-6 col-12 mt-3">
                    <TextField
                      id="outlined-basic"
                      label="UserName"
                      variant="outlined"
                      sx={{ minWidth: "100%" }}
                    />
                  </div>
                  <div className="col-md-6 col-sm-12 col-lg-6 col-12 mt-3">
                    <FormControl sx={{ minWidth: "100%" }}>
                      <InputLabel id="demo-simple-select-helper-label">
                        Role
                      </InputLabel>
                      <Select
                        id="demo-simple-select"
                        // value={age}
                        autoWidth={false}
                        label="Age"
                        sx={{ minWidth: "100%" }}
                        // onChange={handleChange}
                      >
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem>
                      </Select>
                    </FormControl>
                  </div>
                  <div className="col-md-6 col-sm-12 col-lg-6 col-12 mt-3">
                    <TextField
                      id="outlined-basic"
                      label="Password"
                      type={!show_password ? "password" : "text"}
                      // autoComplete="current-password"
                      InputProps={{
                        endAdornment: (
                          <InputAdornment
                            position="end"
                            variant="filled"
                            tabIndex={-1}
                          >
                            <IconButton
                              tabIndex={-1}
                              aria-label="toggle password visibility"
                              onClick={() =>
                                this.setState({
                                  show_password: !show_password,
                                })
                              }
                              edge="end"
                            >
                              {!show_password ? (
                                <VisibilityIcon />
                              ) : (
                                <VisibilityOffIcon />
                              )}
                            </IconButton>
                          </InputAdornment>
                        ),
                      }}
                      variant="outlined"
                      sx={{ minWidth: "100%" }}
                    />
                  </div>
                  <div className="col-md-12 mt-3 mb-3">
                    <TextField
                      id="outlined-basic"
                      label="Email"
                      variant="outlined"
                      sx={{ minWidth: "100%" }}
                    />
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button
                  type="button"
                  class="btn btn-secondary"
                  onClick={() =>
                    this.props.closeModel("add_member_model", false)
                  }
                >
                  Close
                </button>
                <button
                  className="btn float-end button-primary"
                  data-bs-toggle="modal"
                  data-bs-target="#exampleModal"
                >
                  Save Changes
                </button>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default addmember;

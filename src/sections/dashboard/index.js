import React from "react";
import {
  Avatar,
  Divider,
  Stack,
  Menu,
  IconButton,
  MenuItem,
  Typography,
  Card,
  CardHeader,
  CardContent,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import MainPageHeading from "../../utils/enums";
import {
  DollarIcon,
  ScenesIcon,
  TotalMembersIcon,
  TotalAutomationIcon,
} from "../../assets/svg/dashboard";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  AreaChart,
  Area,
  Bar,
  ComposedChart,
  ResponsiveContainer,
  BarChart,
} from "recharts";
import LeftMenu from "./LeftMenu";
import Home from "./home/index";
import Notification from "./notification/index";
import Members from "./members/index";
const data = [
  {
    name: "Page A",
    uv: 4000,
    pv: 2400,
    amt: 2400,
  },
  {
    name: "Page B",
    uv: 3000,
    pv: 1398,
    amt: 2210,
  },
  {
    name: "Page C",
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
  {
    name: "Page D",
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: "Page E",
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: "Page F",
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
  {
    name: "Page G",
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
];

class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = { anchorEl: false, activeTab: 1, };
  }
  getActiveTab = (TabId) => {
    this.setState({
      activeTab: TabId
    })
  }
  onClose = () => {
    this.setState({ anchorEl: !this.state.anchorEl });
  };

  render() {
    const { anchorEl,activeTab } = this.state;
    return (
      <div className="container-fluid position-absolute h-100 w-100 p-0 m-0">
        <div className="sidebar-wrapper">
          <LeftMenu getActiveTab={this.getActiveTab} />
        </div>
        <div className="mainContent">
          <div className="topBar pe-3">
            <div className="float-end">
              <Stack direction="row" spacing={2} alignItems="center">
                <div className="borderLeft"></div>
                <Typography variant="subtitle1" gutterBottom component="div">
                  John Doe
                </Typography>
                <IconButton onClick={this.onClose} size="small" sx={{ ml: 2 }}>
                  <Avatar alt="Cindy Baker" src="/static/images/avatar/3.jpg" />
                </IconButton>
                <Menu
                  anchorEl={anchorEl}
                  open={anchorEl}
                  onClose={this.onClose}
                  onClick={this.onClose}
                  PaperProps={{
                    elevation: 0,
                    sx: {
                      overflow: "visible",
                      filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
                      mt: 4.5,
                      "& .MuiAvatar-root": {
                        width: 32,
                        height: 32,
                        ml: -0.5,
                        mr: 1,
                      },
                      "&:before": {
                        content: '""',
                        display: "block",
                        position: "absolute",
                        top: 0,
                        right: 5,
                        width: 10,
                        height: 10,
                        bgcolor: "background.paper",
                        transform: "translateY(-50%) rotate(45deg)",
                        zIndex: 0,
                      },
                    },
                  }}
                  transformOrigin={{ horizontal: "right", vertical: "top" }}
                  anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
                >
                  <MenuItem>Profile</MenuItem>
                  <MenuItem> My account</MenuItem>
                  <Divider />
                </Menu>
              </Stack>
            </div>
          </div>
          <div className="p-3">
            <div class="tab-content h-100">
              <div className={activeTab == 1 ? "tab-pane fade show active h-100" : "tab-pane fade show h-100"} id="HOME">
                <Home/>
              </div>
              <div className={activeTab == 2 ? "tab-pane fade show active h-100" : "tab-pane fade show h-100"} id="TASKS">
                <Members/>
              </div>
              <div className={activeTab == 3 ? "tab-pane fade show active h-100" : "tab-pane fade show h-100"} id="NOTIFICATIONS">
                 <Notification/>
              </div>
              <div className={activeTab == 5 ? "tab-pane fade show active h-100" : "tab-pane fade show h-100"} id="DASHBOARD">
                  <p>Dashbaord tab content ...</p>
              </div>
              <div className={activeTab == 6 ? "tab-pane fade show active h-100" : "tab-pane fade show h-100"} id="INSIGHT">
                <p>Insight tab content ...</p>
              </div>
              <div className={activeTab == 7 ? "tab-pane fade show active h-100" : "tab-pane fade show h-100"} id="DOCS">
                <p>Docs tab content ...</p>
              </div>
              <div className={activeTab == 8 ? "tab-pane fade show active h-100" : "tab-pane fade show h-100"} id="PULSE">
                <p>Pulse tab content ...</p>
              </div>
              <div className={activeTab == 9 ? "tab-pane fade show active h-100" : "tab-pane fade show h-100"} id="GOALS">
                <p>Goals tab content ...</p>
              </div>
              <div className={activeTab == 10 ? "tab-pane fade show active h-100" : "tab-pane fade show h-100"} id="HELP">
                <p>Help tab content ...</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default index;

const CounterCardObject = [
  {
    id: 0,
    name: "Revenue",
    value: "216k",
    percentage: "+3.48%",
  },
  {
    id: 1,
    name: "Scenes",
    value: "250",
    percentage: "+1.48%",
  },
  {
    id: 2,
    name: "Members",
    value: "450",
    percentage: "+1.48%",
  },
  {
    id: 2,
    name: "Automation",
    value: "50",
    percentage: "+6.48%",
  },
];

import React from "react";
import {
  Avatar,
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
  Typography,
  Card,
  CardHeader,
  CardContent,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import ImageIcon from "@mui/icons-material/Image";
import MainPageHeading from "../../../utils/enums";
import {
  DollarIcon,
  ScenesIcon,
  TotalMembersIcon,
  TotalAutomationIcon,
} from "../../../assets/svg/dashboard";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  AreaChart,
  Area,
  Bar,
  ComposedChart,
  ResponsiveContainer,
  BarChart,
} from "recharts";
const data = [
  {
    name: "Page A",
    uv: 4000,
    pv: 2400,
    amt: 2400,
  },
  {
    name: "Page B",
    uv: 3000,
    pv: 1398,
    amt: 2210,
  },
  {
    name: "Page C",
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
  {
    name: "Page D",
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: "Page E",
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: "Page F",
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
  {
    name: "Page G",
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
];

class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <>
        {MainPageHeading(
          "Activity History",
          "Your activity history is listed as individual items, starting with the most recent"
        )}
        <div className="row p-2">
          <div className="col-12 p-2 activityCard d-flex">
            <h6 className="mt-1">Activities</h6>
            <select className="dashboardSelect float-end ms-3">
              <option>Last 30 Days</option>
              <option>1</option>
              <option>1</option>
            </select>
          </div>
        </div>
        <div className="row p-2">
          <div className="col-12 p-0 m-0">
            <div className="notificationPill">Today</div>
            {CounterCardObject.map((item) => (
              <>
                <ListItem>
                  <ListItemAvatar>
                    <Avatar>
                      <ImageIcon />
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText
                    primary={item.full_name}
                    secondary={item.date}
                  />
                </ListItem>
                <div className="table-responsive marginLeft">
                  <table class="table">
                    <tbody>
                      {item.activities.map((tableRow) => (
                        <tr className="bg-white activityRow">
                          <td>{tableRow.task_name}</td>
                          <td>{tableRow.description}</td>
                          <td>{tableRow.date}</td>
                          <td>{tableRow.time}</td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </>
            ))}
            <hr />
          </div>
        </div>
      </>
    );
  }
}

export default index;

const CounterCardObject = [
  {
    id: 0,
    full_name: "Malik Muhammad Waleed Ahmed",
    date: "20 Nov 2021",
    activities: [
      {
        id: 0,
        task_name: "Bulb Activated",
        description: "Integrated a New Light Bulb at Room No 1",
        date: "20 Nov 2021",
        time: "10:52 PM",
        task_type: 0,
      },
    ],
  },
  {
    id: 1,
    date: "20 Nov 2021",
    full_name: "Bilal Asghar",
    activities: [
      {
        id: 0,
        task_name: "Back Door Activated",
        description: "Integrated a New Light Bulb at Room No 1",
        date: "20 Nov 2021",
        time: "10:52 PM",
        task_type: 0,
      },
      {
        id: 1,
        task_name: "Back Door Activated",
        description: "Integrated a New Light Bulb at Room No 1",
        date: "20 Nov 2021",
        time: "10:52 PM",
        task_type: 0,
      },
    ],
  },
];

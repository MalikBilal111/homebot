import React from "react";
import {
  Avatar,
  Divider,
  Stack,
  Menu,
  IconButton,
  MenuItem,
  Typography,
  Card,
  CardHeader,
  CardContent,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import MainPageHeading from "../../../utils/enums";
import {
  DollarIcon,
  ScenesIcon,
  TotalMembersIcon,
  TotalAutomationIcon,
} from "../../../assets/svg/dashboard";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  AreaChart,
  Area,
  Bar,
  ComposedChart,
  ResponsiveContainer,
  BarChart,
} from "recharts";
const data = [
  {
    name: "Page A",
    uv: 4000,
    pv: 2400,
    amt: 2400,
  },
  {
    name: "Page B",
    uv: 3000,
    pv: 1398,
    amt: 2210,
  },
  {
    name: "Page C",
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
  {
    name: "Page D",
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: "Page E",
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: "Page F",
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
  {
    name: "Page G",
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
];

class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <>
        {MainPageHeading("Good Morning Mate!", "Have a nice day")}
        <div className="row">
          {CounterCardObject &&
            CounterCardObject.map((item) => (
              <div className="col-md-3 col-lg-2 col-sm-12 counterCard mt-2">
                <div class="card">
                  <CardHeader
                    action={
                      <IconButton aria-label="settings">
                        {item.name == "Revenue" ? (
                          <DollarIcon />
                        ) : item.name == "Scenes" ? (
                          <ScenesIcon />
                        ) : item.name == "Members" ? (
                          <TotalMembersIcon />
                        ) : (
                          <TotalAutomationIcon />
                        )}
                      </IconButton>
                    }
                    title={item.value}
                    subheader={item.name}
                  />
                  <div class="card-body">
                    <p>{item.percentage}</p>
                  </div>
                </div>
              </div>
            ))}
        </div>
        <div className="row">
          <div className="col-md-6 col-lg-6 col-sm-12 dashboardMainCard mt-2">
            <div class="card">
              <div class="card-body p-3">
                <div>
                  <h5 class="card-title float-start">Weather</h5>
                  <select className="dashboardSelect float-end">
                    <option>Last 30 Days</option>
                    <option>1</option>
                    <option>1</option>
                  </select>
                  <br />
                </div>
                <div style={{ height: "200px" }}>
                  <ResponsiveContainer width="100%" height="100%">
                    <ComposedChart
                      width={500}
                      height={400}
                      data={data}
                      margin={{
                        top: 20,
                        right: 20,
                        bottom: 20,
                        left: 20,
                      }}
                    >
                      <CartesianGrid strokeDasharray="3 3" />
                      <Bar dataKey="pv" barSize={20} fill="#F0F2F8" />
                      <Tooltip />
                      <Line
                        type="monotone"
                        dataKey="pv"
                        stroke="#A3A0FB"
                        strokeWidth={2}
                      />
                    </ComposedChart>
                  </ResponsiveContainer>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6 col-lg-6 col-sm-12 dashboardMainCard mt-2">
            <div class="card">
              <div class="card-body p-3">
                <div>
                  <h5 class="card-title float-start">Power Usage</h5>
                  <select className="dashboardSelect float-end">
                    <option>Last 30 Days</option>
                    <option>1</option>
                    <option>1</option>
                  </select>
                  <br />
                </div>
                <div style={{ height: "200px" }}>
                  <ResponsiveContainer width="100%" height="100%">
                    <ComposedChart
                      width={500}
                      height={400}
                      data={data}
                      margin={{
                        top: 20,
                        right: 20,
                        bottom: 20,
                        left: 20,
                      }}
                    >
                      <CartesianGrid strokeDasharray="3 3" />
                      <Bar dataKey="pv" barSize={20} fill="#FCE7ED" />
                      <Tooltip />
                      <Line
                        type="monotone"
                        dataKey="pv"
                        stroke="#E21950"
                        strokeWidth={2}
                      />
                    </ComposedChart>
                  </ResponsiveContainer>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-3 col-lg-3 col-sm-12 dashboardMainCard mt-2">
            <div class="card">
              <div class="card-body p-3">
                <div>
                  <h5 class="card-title float-start">Spending</h5>
                  <select className="dashboardSelect float-end">
                    <option>Last 30 Days</option>
                    <option>1</option>
                    <option>1</option>
                  </select>
                </div>
                <div style={{ height: "200px" }}>
                  <ResponsiveContainer width="100%" height="100%">
                    <BarChart
                      width={500}
                      height={300}
                      data={data}
                      margin={{
                        top: 20,
                        right: 30,
                        left: 20,
                        bottom: 5,
                      }}
                    >
                      <CartesianGrid strokeDasharray="3 3" />
                      <Tooltip />
                      <Legend />
                      <Bar dataKey="pv" stackId="a" fill="#8884d8" />
                      <Bar dataKey="amt" stackId="a" fill="#82ca9d" />
                      <Bar dataKey="uv" fill="#ffc658" />
                    </BarChart>
                  </ResponsiveContainer>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-3 col-lg-3 col-sm-12 dashboardMainCard mt-2">
            <div class="card">
              <div class="card-body p-3">
                <div>
                  <h5 class="card-title float-start">Recent Active</h5>
                  <select className="dashboardSelect float-end">
                    <option>Last 30 Days</option>
                    <option>1</option>
                    <option>1</option>
                  </select>
                </div>
                <div
                  style={{
                    height: "200px",
                    overflowY: "auto",
                    width: "100%",
                  }}
                >
                  <TableContainer>
                    <Table>
                      <TableHead>
                        <TableRow>
                          <TableCell>Employee Name</TableCell>
                          <TableCell>Date</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        <TableRow>
                          <TableCell>Waleed Ahmed</TableCell>
                          <TableCell>26 Oct 2021</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Bilal Asghar</TableCell>
                          <TableCell>26 Oct 2021</TableCell>
                        </TableRow>
                      </TableBody>
                    </Table>
                  </TableContainer>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-3 col-lg-3 col-sm-12 dashboardMainCard mt-2">
            <div class="card">
              <div class="card-body p-3">
                <div>
                  <h5 class="card-title float-start">Spending</h5>
                  <select className="dashboardSelect float-end">
                    <option>Last 30 Days</option>
                    <option>1</option>
                    <option>1</option>
                  </select>
                </div>
                <div style={{ height: "200px" }}>
                  <ResponsiveContainer width="100%" height="100%">
                    <BarChart
                      width={500}
                      height={300}
                      data={data}
                      margin={{
                        top: 20,
                        right: 30,
                        left: 20,
                        bottom: 5,
                      }}
                    >
                      <CartesianGrid strokeDasharray="3 3" />
                      <Tooltip />
                      <Legend />
                      <Bar dataKey="pv" stackId="a" fill="#8884d8" />
                      <Bar dataKey="amt" stackId="a" fill="#82ca9d" />
                      <Bar dataKey="uv" fill="#ffc658" />
                    </BarChart>
                  </ResponsiveContainer>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-3 col-lg-3 col-sm-12 dashboardMainCard mt-2">
            <div class="card">
              <div class="card-body p-3">
                <div>
                  <h5 class="card-title float-start">Recent Active</h5>
                  <select className="dashboardSelect float-end">
                    <option>Last 30 Days</option>
                    <option>1</option>
                    <option>1</option>
                  </select>
                </div>
                <div
                  style={{
                    height: "200px",
                    overflowY: "auto",
                    width: "100%",
                  }}
                >
                  <TableContainer>
                    <Table>
                      <TableHead>
                        <TableRow>
                          <TableCell>Employee Name</TableCell>
                          <TableCell>Date</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        <TableRow>
                          <TableCell>Waleed Ahmed</TableCell>
                          <TableCell>26 Oct 2021</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>Bilal Asghar</TableCell>
                          <TableCell>26 Oct 2021</TableCell>
                        </TableRow>
                      </TableBody>
                    </Table>
                  </TableContainer>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12 dashboardMainCard mt-2">
            <div class="card">
              <div class="card-body p-1">
                <Stack direction="row" spacing={0}>
                  <h5 className="dashobardLatestMemberTitle">
                    Latest Members:
                  </h5>
                  <IconButton>
                    <Avatar
                      alt="Remy Sharp"
                      src="/static/images/avatar/1.jpg"
                    />
                  </IconButton>
                  <IconButton>
                    <Avatar
                      alt="Travis Howard"
                      src="/static/images/avatar/2.jpg"
                    />
                  </IconButton>
                  <IconButton>
                    <Avatar
                      alt="Cindy Baker"
                      src="/static/images/avatar/3.jpg"
                    />
                  </IconButton>
                  <IconButton>
                    <Avatar
                      alt="Cindy Baker"
                      src="/static/images/avatar/3.jpg"
                    />
                  </IconButton>
                  <IconButton>
                    <Avatar
                      alt="Cindy Baker"
                      src="/static/images/avatar/3.jpg"
                    />
                  </IconButton>
                  <IconButton>
                    <Avatar
                      alt="Cindy Baker"
                      src="/static/images/avatar/3.jpg"
                    />
                  </IconButton>
                  <IconButton>
                    <Avatar
                      alt="Cindy Baker"
                      src="/static/images/avatar/3.jpg"
                    />
                  </IconButton>
                </Stack>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default index;

const CounterCardObject = [
  {
    id: 0,
    name: "Revenue",
    value: "216k",
    percentage: "+3.48%",
  },
  {
    id: 1,
    name: "Scenes",
    value: "250",
    percentage: "+1.48%",
  },
  {
    id: 2,
    name: "Members",
    value: "450",
    percentage: "+1.48%",
  },
  {
    id: 2,
    name: "Automation",
    value: "50",
    percentage: "+6.48%",
  },
];

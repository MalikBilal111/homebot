import React from 'react'
import {menuItems,bottomMenu} from '../../utils/menuobject' // for menu
// import logo from '../assets/img/logo.png'
import Tooltip from '@material-ui/core/Tooltip';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
// import HelpPopUp from './help/index';

class LeftMenu extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
        anchorEl:false,
        activeMenu:1,
        menuItems: menuItems,
        bottomMenuItems:bottomMenu,
        helpTrigger:false,
        helpKey:100,
    }
  }


  handleClick=()=>
  {
    this.setState({
        anchorEl:!this.state.anchorEl,
    })
  }
  changeActiveMenu=(value)=>
  {
    if(value==10)
    {
      this.setState({
        helpKey:value,
        helpTrigger:true,
      })
    }
    else
    {
      this.setState({
        activeMenu:value,
        helpKey:100,
      })
    this.props.getActiveTab(value);
    }
    
  }

  toggleHelpPopUp=()=>
  {
    this.setState({
      helpTrigger:!this.state.helpTrigger,
      helpKey:100,
    })
  }
  
  render() {
    const {anchorEl,activeMenu,menuItems,bottomMenuItems,helpKey,helpTrigger}=this.state;
    return (
        <>
    {/* {
      helpTrigger==true
      ?
      <HelpPopUp closePopUp={this.toggleHelpPopUp}/>
      :
      null
    } */}

            <div id = "sidebar-wrapper" class = "sidebar-toggle">
                {/* <img src={logo} width="40" height="40" /> */}
                <ul className = "sidebar-nav nav nav-tabs">
                {menuItems.map((item) => (
                  item.name!="DIVIDER"
                  ?
                  <Tooltip placement="right" title={item.name} arrow>
                                <li className="nav-item">
                                    <div className={activeMenu==item.key?"menuItem activeMenuItem":"menuItem"} onClick={()=>this.changeActiveMenu(item.key)}>
                                        <a className="nav-link" data-toggle="tab">
                                        {
                                            activeMenu==item.key
                                            ?
                                            item.secondaryIcon
                                            :
                                            item.icon
                                        }
                                        </a>
                                    </div>
                                </li>
                            </Tooltip>
                  :
                  <li className="w-100"><hr/></li>
                ))}
                
                   
                </ul>
                <ul className = "sidebar-nav-bottom">
                {bottomMenuItems.map((item) => {
                  if (item.name !== "DIVIDER") {
                    return (
                      <>
                        {
                          item.name=="HELP"
                          ?
                          <li>
                                    <div className={(activeMenu==item.key || helpKey==item.key)?"menuItem activeMenuItem":"menuItem"} onClick={()=>this.changeActiveMenu(item.key)}>
                                        {
                                            (activeMenu==item.key || helpKey==item.key)
                                            ?
                                            item.secondaryIcon
                                            :
                                            item.icon
                                        }
                                    </div>
                                </li>
                          :
                          <Tooltip placement="right" title={item.name} arrow>
                                <li>
                                    <div className={(activeMenu==item.key || helpKey==item.key)?"menuItem activeMenuItem":"menuItem"} onClick={()=>this.changeActiveMenu(item.key)}>
                                        {
                                            (activeMenu==item.key || helpKey==item.key)
                                            ?
                                            item.secondaryIcon
                                            :
                                            item.icon
                                        }
                                    </div>
                                </li>
                            </Tooltip>

                        }
                            
                      </>
                    )
                  } else {
                    return (
                      <>
                         <li className="w-100"><hr/></li>
                      </>
                    )
                  }
                })}
                {/* <li>
                    <div className="menuItem" aria-controls="simple-menu" aria-haspopup="true" onClick={this.handleClick} >
                        <div><MoreVertOutlinedIcon fontSize="small"/>
                        <Menu id="simple-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={this.handleClick}>
                            <MenuItem onClick={this.handleClick}>Reporting</MenuItem>
                            <MenuItem onClick={this.handleClick}>My account</MenuItem>
                        </Menu>
                        </div>
                    </div>
                   
                </li>  */}
                    <li><hr className="m-1"/></li>
                    <li><div className="menuItem profileIcon"><b>B</b></div></li>
                </ul>
            </div> 
        </>
    )
  }
}

export default LeftMenu;

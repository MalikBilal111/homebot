import React from "react";
import "../../styles/login.css";
// import axios from 'axios';
import {
  TextField,
  InputAdornment,
  IconButton,
  Button,
  Grid,
  Typography,
  ButtonGroup,
  ButtonBase,
} from "@mui/material";
import { runValidator } from "../../utils/validations";
import clonedeep from "lodash.clonedeep";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import FacebookIcon from "@mui/icons-material/Facebook";
import GoogleIcon from "@mui/icons-material/Google";
import { withRouter } from "react-router";
class index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      TwoFactor: false,
      RightPanelActive: false,
      showPassword: false,
      errors: {},
    };
    this.INPUTS = {
      email: {
        rules: { required: true, email: true },
        errorMessage: "Enter a valid email address",
      },
      password: {
        rules: { required: true },
        errorMessage: "Enter the password",
      },
    };
  }

  logInUser = () => {
    this.props.history.push("/dashboard");
  };

  changeView = () => {
    this.setState({
      RightPanelActive: !this.state.RightPanelActive,
    });
  };

  handleValidation = (e) => {
    // let errors = runValidator(e.target.value, rules)
    let errors = runValidator(e.target.value, this.INPUTS[e.target.name].rules);
    let errorMessage = this.INPUTS[e.target.name].errorMessage;
    if (errors.length > 0) {
      let uppdatedErrors = clonedeep(this.state.errors);

      this.setState({
        errors: { ...uppdatedErrors, [e.target.name]: errorMessage },
      });
    } else {
      let uppdatedErrors = clonedeep(this.state.errors);
      delete uppdatedErrors[e.target.name];
      this.setState({ errors: uppdatedErrors });
    }
  };

  clearError = (e) => {
    if (this.state.errors[e.target.name]) {
      let uppdatedErrors = clonedeep(this.state.errors);
      delete uppdatedErrors[e.target.name];
      this.setState({ errors: uppdatedErrors });
    }
  };

  render() {
    const { RightPanelActive, showPassword, errors } = this.state;
    let { classes } = this.props;
    console.log("CLASSES", classes);
    return (
      <>
        <Grid
          container
          spacing={0}
          direction="row"
          justifyContent="center"
          alignItems="center"
          className="container-page"
        >
          <div
            className={
              RightPanelActive
                ? "container LoginPage right-panel-active"
                : "container LoginPage"
            }
            id="container"
          >
            <div className="form-container sign-up-container">
              <form action="#">
                <Typography className="title" variant="h3" component="div">
                  Create Account
                </Typography>
                <div className="social-container">
                  <ButtonGroup variant="text" aria-label="text button group">
                    <IconButton>
                      <FacebookIcon />
                    </IconButton>
                    <IconButton>
                      <GoogleIcon />
                    </IconButton>
                  </ButtonGroup>
                </div>
                <Typography variant="caption">
                  or use your email for registration
                </Typography>
                <div className="fieldContainer">
                  <TextField
                    id="outlined-basic"
                    label="Name"
                    variant="outlined"
                    margin="dense"
                    className="input-field"
                  />
                  <TextField
                    id="outlined-basic"
                    label="Email"
                    variant="outlined"
                    margin="dense"
                    className="input-field"
                  />
                  <TextField
                    id="outlined-basic"
                    label="Password"
                    variant="outlined"
                    margin="dense"
                    className="input-field"
                    type={showPassword ? "text" : "password"}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment
                          position="end"
                          variant="filled"
                          tabIndex={-1}
                        >
                          <IconButton
                            tabIndex={-2}
                            aria-label="toggle password visibility"
                            onClick={() =>
                              this.setState({
                                showPassword: !this.state.showPassword,
                              })
                            }
                            edge="end"
                          >
                            {!this.state.showPassword ? (
                              <VisibilityIcon />
                            ) : (
                              <VisibilityOffIcon />
                            )}
                          </IconButton>
                        </InputAdornment>
                      ),
                    }}
                  />
                  <Button
                    onClick={this.logInUser}
                    variant="contained"
                    disableElevation
                  >
                    Sign Up
                  </Button>
                </div>
              </form>
            </div>
            <Grid container spacing={2}>
              <div className="form-container sign-in-container">
                <form action="#">
                  <Typography className="title" variant="h3" component="div">
                    Sign in
                  </Typography>
                  <div className="social-container">
                    <ButtonGroup variant="text" aria-label="text button group">
                      <IconButton>
                        <FacebookIcon />
                      </IconButton>
                      <IconButton>
                        <GoogleIcon />
                      </IconButton>
                    </ButtonGroup>
                  </div>
                  <Typography variant="caption">or use your account</Typography>
                  <TextField
                      id="outlined-basic"
                      label="Email"
                      variant="outlined"
                      className="input-field"
                      margin="dense"
                      name="email"
                      onBlur={this.handleValidation}
                      onFocus={this.clearError}
                      error={errors.email ? true : false}
                      // helperText={errors.email && errors.email}
                    />
                  <div className="fieldContainer">
                    
                    <TextField
                      id="outlined-basic"
                      label="Password"
                      className="input-field"
                      variant="outlined"
                      margin="dense"
                      name="password"
                      type={showPassword ? "text" : "password"}
                      error={errors.password ? true : false}
                      helperText={errors.password && errors.password}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment
                            position="end"
                            variant="filled"
                            tabIndex={-1}
                          >
                            <IconButton
                              tabIndex={-2}
                              aria-label="toggle password visibility"
                              onClick={() =>
                                this.setState({
                                  showPassword: !this.state.showPassword,
                                })
                              }
                              edge="end"
                            >
                              {!this.state.showPassword ? (
                                <VisibilityIcon />
                              ) : (
                                <VisibilityOffIcon />
                              )}
                            </IconButton>
                          </InputAdornment>
                        ),
                      }}
                    />
                    <a href="#">Forgot your password?</a>
                    <Button
                      onClick={this.logInUser}
                      variant="contained"
                      disableElevation
                      fullWidth={true}
                    >
                      Sign In
                    </Button>
                  </div>
                  {/* <button onClick={this.logInUser}>Sign In</button> */}
                </form>
              </div>
            </Grid>
            <div className="overlay-container">
              <div className="overlay">
                <div className="overlay-panel overlay-left">
                  <Typography className="title" variant="h3" component="div">
                    Welcome Back!
                  </Typography>
                  <p className="overlay-para">
                    To keep connected with us please login with your personal
                    info
                  </p>
                  <Button
                    onClick={this.changeView}
                    variant="contained"
                    disableElevation
                  >
                    Sign In
                  </Button>
                </div>
                <div className="overlay-panel overlay-right">
                  <Typography className="title" variant="h2" component="div">
                    HomeBot
                  </Typography>
                  <Typography
                    className="{classes.linkText}"
                    variant="h6"
                    gutterBottom
                    component="subtitle1"
                  >
                    {" "}
                    Create your perfect home experience.
                  </Typography>
                  <Typography variant="caption">
                    With HomeBot, you decide how all your devices at home work
                    together.
                    <br />
                    Let your creativity flow and create a home that feels like
                    you.
                  </Typography>
                  <Button
                    onClick={this.changeView}
                    variant="contained"
                    disableElevation
                  >
                    Sign Up
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </Grid>
      </>
    );
  }
}

export default withRouter(index)
// export default index;

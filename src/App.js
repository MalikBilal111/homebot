import "../src/styles/styles.css";
// import MainScreen from './components/MainScreen';
// import LoginScreen from './components/login/index';
import LoginScreen from "../src/sections/login/index";
import DashboardScren from "../src/sections/dashboard/index";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { createTheme, ThemeProvider } from "@material-ui/core/styles";
import { purple, deepPurple } from "@mui/material/colors";

const theme = createTheme({
  overrides: {
    MuiPaginationItem: {
      page: { backgroundColor: "#fff" },
    },
    MuiAlert: {
      root: {
        padding: "0",
        boxShadow: "0px 15px 33px -19px rgba(0,0,0,0.75)",
        backgroundColor: "#fff",
        borderRadius: "10px",
      },
      standardSuccess: {
        backgroundColor: "#fff",
      },
      standardError: {
        backgroundColor: "#fff",
      },
      icon: {
        padding: "15px",
        fontSize: "33px",
        marginRight: "2rem",
      },
      message: {
        padding: "0",
        lineHeight: "3.7rem",
      },
      action: {
        marginRight: "4px",
      },
    },
  },
  typography: {
    fontFamily: [
      "Lato",
      "TTNorms",
      "Roboto",
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
  },
  palette: {
    primary: {
      main: "#ff99ff",
      light: "#2680be",
      dark: "#005a98",
      contrastText: "#fff",
    },
    secondary: {
      main: "#FC443E",
      dark: "#EC3A34",
      light: "#FB5954",
      contrastText: "#fff",
    },
  },
});
function App({ props }) {
  return (
    <>
      <Router>
        <Route exact path="/">
          <LoginScreen />
        </Route>
        <Route exact path="/dashboard">
          <DashboardScren />
        </Route>
      </Router>
    </>
  );
}

export default App;

import DashboardOutlinedIcon from '@material-ui/icons/DashboardOutlined';
import DashboardIcon from '@material-ui/icons/Dashboard';
import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';
import CheckBoxOutlinedIcon from '@material-ui/icons/CheckBoxOutlined';
import NotificationsNoneOutlinedIcon from '@material-ui/icons/NotificationsNoneOutlined';
import MoreVertOutlinedIcon from '@material-ui/icons/MoreVertOutlined';
import HelpOutlineOutlinedIcon from '@material-ui/icons/HelpOutlineOutlined';
import DescriptionOutlinedIcon from '@material-ui/icons/DescriptionOutlined';
import AssessmentOutlinedIcon from '@material-ui/icons/AssessmentOutlined';
import CastConnectedOutlinedIcon from '@material-ui/icons/CastConnectedOutlined';
import EmojiEventsOutlinedIcon from '@material-ui/icons/EmojiEventsOutlined';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import NotificationsIcon from '@material-ui/icons/Notifications';
import EmojiEventsIcon from '@material-ui/icons/EmojiEvents';
import HelpIcon from '@material-ui/icons/Help';
import CastConnectedIcon from '@material-ui/icons/CastConnected';
import DescriptionIcon from '@material-ui/icons/Description';
import AssessmentIcon from '@material-ui/icons/Assessment';
import HomeIcon from '@material-ui/icons/Home';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
const menuItems = [
  {
    name: 'HOME',
    key: 1,
    icon: <HomeOutlinedIcon fontSize="small"/>,
    secondaryIcon:<HomeIcon fontSize="small"/>,
  },
  {
    name: 'Members',
    key: 2,
    icon: <SupervisorAccountIcon fontSize="small"/>,
    secondaryIcon:<SupervisorAccountIcon fontSize="small"/>,

  },
  {
    name: 'NOTIFICATIONS',
    key: 3,
    icon: <NotificationsNoneOutlinedIcon fontSize="small"/>,
    secondaryIcon:<NotificationsIcon fontSize="small"/>,

  },
  {
    name: 'DIVIDER',
    key: 4,
    icon: 'NA',
    secondaryIcon:"NA",
  },
  {
    name: 'DASHBOARD',
    key: 5,
    icon: <DashboardOutlinedIcon fontSize="small"/>,
    secondaryIcon:<DashboardIcon fontSize="small"/>,

  },
  {
    name: 'INSIGHT',
    key: 6,
    icon: <AssessmentOutlinedIcon fontSize="small"/>,
    secondaryIcon:<AssessmentIcon fontSize="small"/>,

  },
]
const bottomMenu = [
  {
    name: 'DOCS',
    key: 7,
    icon: <DescriptionOutlinedIcon fontSize="small"/>,
    secondaryIcon:<DescriptionIcon fontSize="small"/>,

  },
  {
    name: 'PULSE',
    key: 8,
    icon: <CastConnectedOutlinedIcon fontSize="small"/>,
    secondaryIcon:<CastConnectedIcon fontSize="small"/>,

  },
  {
    name: 'GOALS',
    key: 9,
    icon: <EmojiEventsOutlinedIcon fontSize="small"/>,
    secondaryIcon:<EmojiEventsIcon fontSize="small"/>,
  },
  {
    name: 'HELP',
    key: 10,
    icon: <HelpOutlineOutlinedIcon fontSize="small"/>,
    secondaryIcon:<HelpIcon fontSize="small"/>,
  },
]

export {menuItems,bottomMenu}

function MainPageHeading(heading,subtitle) {
  return (
    <>
      <h3 className="MainPageHeading-heading">{heading}</h3>
      {subtitle?<h6 className="MainPageHeading-subtitle">{subtitle}</h6>:null}
    </>
  );
}

export default MainPageHeading;
